<?php

use Itgro\LaravelConfirmations\Http\Controllers\Confirmations as Controller;
use Itgro\LaravelConfirmations\Models\Redirect;

return [

    'table' => 'confirmations',

    'models' => [
        'user' => 'App\User',
    ],

    'actions' => [
        Redirect::class,
    ],

    'channels' => [

    ],

    'routes' => [
        'controller' => Controller::class,
        'prefix' => 'confirmations',
        'middleware' => ['bindings'],
        'form' => [
            'middleware' => [],
        ],
        'check' => [
            'middleware' => [],
        ],
        'send' => [
            'middleware' => [],
        ],
    ],

    'default_redirect' => [
        'route' => 'home',
        'parameters' => [],
        'anchor' => '',
    ],

];
