<?php

namespace Itgro\LaravelConfirmations\Channels;

use Itgro\LaravelConfirmations\Models\Confirmation;

abstract class Contract
{
    public const IDENTIFIER = null;

    abstract public static function generateToken(): string;

    public function getIdentifier(): string
    {
        return static::IDENTIFIER;
    }

    abstract public function getContact($user): string;

    abstract public function send(Confirmation $confirmation): bool;

    abstract public function toMessage(Confirmation $confirmation);
}
