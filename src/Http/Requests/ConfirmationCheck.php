<?php

namespace Itgro\LaravelConfirmations\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Itgro\LaravelConfirmations\Models\Confirmation;
use Itgro\LaravelConfirmations\Rules\ConfirmationCode;

class ConfirmationCheck extends FormRequest
{
    public function authorize()
    {
        return $this->getModel()->authorize();
    }

    public function rules()
    {
        return [
            'token' => [
                'required',
                new ConfirmationCode($this->getModel()),
            ],
        ];
    }

    private function getModel(): Confirmation
    {
        /** @var Confirmation|string $model */
        $model = $this->route('confirmation');

        if (is_string($model)) {
            $model = Confirmation::findOrFail($model);
        }

        return $model;
    }
}
