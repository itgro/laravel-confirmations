<?php

namespace Itgro\LaravelConfirmations\Http\Controllers;

use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;
use Itgro\LaravelConfirmations\Exceptions\AlreadyExecuted;
use Itgro\LaravelConfirmations\Exceptions\Expired;
use Itgro\LaravelConfirmations\Exceptions\UserMismatch;
use Itgro\LaravelConfirmations\Http\Requests\ConfirmationCheck;
use Itgro\LaravelConfirmations\Models\Confirmation;
use Throwable;

class Confirmations extends BaseController
{
    public function form(Confirmation $confirmation, Request $request): View
    {
        $prefilled = $request->has('token');

        return view('confirmations::form', compact('confirmation', 'prefilled'));
    }

    public function check(Confirmation $confirmation): RedirectResponse
    {
        // Нужно произвести валидацию через Form Request, но нам не нужен сам объект запроса.
        // Поэтому вызываем валидацию вручную, а не через параметры этого метода.
        app(ConfirmationCheck::class);

        $transPrefix = "confirmations::messages.{$confirmation->action}";

        try {
            if ($confirmation->execute()) {
                $text = trans("{$transPrefix}.success");
                $level = 'success';
            } else {
                $text = trans("{$transPrefix}.failure");
                $level = 'error';
            }
        } catch (AlreadyExecuted $e) {
            $text = trans("{$transPrefix}.already_executed");
            $level = 'error';
        } catch (Expired $e) {
            $text = trans("{$transPrefix}.expired");
            $level = 'error';
        } catch (UserMismatch $e) {
            $text = trans("{$transPrefix}.access_denied");
            $level = 'error';
        } catch (Throwable $e) {
            $text = trans("{$transPrefix}.unexpected_failure", ['message' => $e->getMessage()]);
            $level = 'error';
        }

        $this->flash($text, $level);

        return $confirmation->redirect();
    }

    public function send(Confirmation $confirmation): RedirectResponse
    {
        $response = redirect()->route('confirmations::form', $confirmation);

        try {
            if ($confirmation->send()) {
                $this->flash(trans("confirmations::messages.sent"), 'success');
            } else {
                $this->flash(trans("confirmations::messages.not_sent"), 'error');
            }
        } catch (ThrottleRequestsException $e) {
            $headers = $e->getHeaders();
            $time = $headers['Retry-After'];

            $response
                ->withHeaders($headers)
                ->withErrors([
                    'throttle' => trans(
                        "confirmations::messages.{$confirmation->action}.form_resend_delay",
                        ['delay' => $time . ' ' . choose_declension($time, 'секунду', 'секунды', 'секунд')]
                    ),
                ]);
        }

        return $response;
    }

    protected function flash(string $text, string $level): void
    {
        if ($text) {
            session()->flash('confirmations_message', compact('text', 'level'));
        }
    }
}
