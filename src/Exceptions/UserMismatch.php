<?php

namespace Itgro\LaravelConfirmations\Exceptions;

use Exception;

class UserMismatch extends Exception
{
}
