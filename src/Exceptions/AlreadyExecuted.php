<?php

namespace Itgro\LaravelConfirmations\Exceptions;

use Exception;

class AlreadyExecuted extends Exception
{
}
