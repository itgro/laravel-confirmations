<?php

namespace Itgro\LaravelConfirmations\Exceptions;

use InvalidArgumentException;

class InvalidContact extends InvalidArgumentException
{
}
