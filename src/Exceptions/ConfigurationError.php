<?php

namespace Itgro\LaravelConfirmations\Exceptions;

use Exception;

class ConfigurationError extends Exception
{
    public static function invalidUserObject(
        /** @noinspection PhpUnusedParameterInspection */
        $user
    ): self {
        return new static('Invalid user object');
    }

    public static function invalidChannelClass($class): self
    {
        return new static("Invalid Channel class {$class}");
    }

    public static function invalidConfirmationClass($class): self
    {
        return new static("Invalid Confirmation class {$class}");
    }

    public static function channelNotRegistered($identifier)
    {
        return new static("Channel {$identifier} is not registered");
    }
}
