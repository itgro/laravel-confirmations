<?php

namespace Itgro\LaravelConfirmations;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Itgro\LaravelConfirmations\Models\Confirmation;

class ServiceProvider extends BaseServiceProvider
{
    protected $defer = false;
    protected $namespace = 'confirmations';

    public function boot()
    {
        $this->config();
        $this->migrations();
        $this->views();
        $this->routes();
        $this->translations();
    }

    public function register()
    {
        $this->registerActions();
        $this->registerChannels();
        $this->registerHelpers();
    }

    protected function config()
    {
        $path = __DIR__ . '/../config/confirmations.php';

        $this->mergeConfigFrom($path, $this->namespace);

        $this->publishes([
            $path => config_path('confirmations.php'),
        ], 'config');
    }

    protected function migrations()
    {
        if (class_exists('CreateConfirmationsTable')) {
            return;
        }

        $timestamp = date('Y_m_d_His', time());

        $this->publishes([
            __DIR__ . '/../database/migrations/create_confirmations_table.php' => $this->app->databasePath() . "/migrations/{$timestamp}_create_confirmations_table.php",
        ], 'migrations');
    }

    protected function views()
    {
        $path = __DIR__ . '/../resources/views';

        $this->loadViewsFrom($path, $this->namespace);

        $this->publishes([
            $path => resource_path('views/vendor/confirmations'),
        ], 'views');
    }

    protected function routes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }

    protected function translations()
    {
        $path = __DIR__ . '/../resources/lang';

        $this->loadTranslationsFrom($path, $this->namespace);

        $this->publishes([
            $path => resource_path('lang/vendor/confirmations'),
        ], 'lang');
    }

    protected function registerActions()
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        Confirmation::registerAction(config('confirmations.actions'));
    }

    protected function registerChannels()
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        Confirmation::registerChannel(config('confirmations.channels'));
    }

    protected function registerHelpers()
    {
        require __DIR__ . '/helpers.php';
    }
}
