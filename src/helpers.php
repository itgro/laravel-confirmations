<?php

use Itgro\LaravelConfirmations\Models\Confirmation;

if (!function_exists('link_to_confirmation')) {
    function link_to_confirmation(Confirmation $action, ?string $title = null, $parameters = [], $attributes = [], bool $prefilled = false): string
    {
        if ($prefilled) {
            $parameters['token'] = $action->token;
        }

        return link_to_route('confirmations::check', $title, $parameters, $attributes);
    }
}

if (!function_exists('link_to_prefilled_confirmation')) {
    function link_to_prefilled_confirmation(Confirmation $action, ?string $title = null, $parameters = [], $attributes = []): string
    {
        return link_to_confirmation($action, $title, $parameters, $attributes, true);
    }
}
