<?php

namespace Itgro\LaravelConfirmations\Events;

use Itgro\LaravelConfirmations\Models\Confirmation;

class Failed
{
    public $action;
    public $reason;

    public function __construct(Confirmation $action, string $reason = '')
    {
        $this->action = $action;
        $this->reason = $reason;
    }
}
