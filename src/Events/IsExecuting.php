<?php

namespace Itgro\LaravelConfirmations\Events;

use Itgro\LaravelConfirmations\Models\Confirmation;

class IsExecuting
{
    public $action;

    public function __construct(Confirmation $action)
    {
        $this->action = $action;
    }
}
