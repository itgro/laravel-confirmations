<?php

namespace Itgro\LaravelConfirmations\Events;

use Itgro\LaravelConfirmations\Models\Confirmation;

class WasExecuted
{
    public $action;

    public function __construct(Confirmation $action)
    {
        $this->action = $action;
    }
}
