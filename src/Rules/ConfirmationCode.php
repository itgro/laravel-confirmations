<?php

namespace Itgro\LaravelConfirmations\Rules;

use Illuminate\Contracts\Validation\Rule;
use Itgro\LaravelConfirmations\Models\Confirmation;

class ConfirmationCode implements Rule
{
    private $confirmation;
    private $message;

    public function __construct(Confirmation $confirmation)
    {
        $this->confirmation = $confirmation;
    }

    public function passes($attribute, $value): bool
    {
        if (!$this->confirmation->can_be_executed) {
            $this->message = 'Повторное выполнение действия невозможно';
            return false;
        }

        if ($this->confirmation->is_expired) {
            $this->message = 'Время выполнения действия истекло';
            return false;
        }

        if (!$this->confirmation->token || $this->confirmation->token !== $value) {
            $this->message = 'Введён неверный код';
            return false;
        }

        return true;
    }

    public function message()
    {
        return $this->message;
    }
}
