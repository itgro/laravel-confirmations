<?php

namespace Itgro\LaravelConfirmations\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Itgro\LaravelConfirmations\Channels\Contract as Channel;
use Itgro\LaravelConfirmations\Events\Failed;
use Itgro\LaravelConfirmations\Events\IsExecuting;
use Itgro\LaravelConfirmations\Events\WasExecuted;
use Itgro\LaravelConfirmations\Exceptions\AlreadyExecuted;
use Itgro\LaravelConfirmations\Exceptions\ConfigurationError;
use Itgro\LaravelConfirmations\Exceptions\Expired;
use Itgro\LaravelConfirmations\Exceptions\InvalidContact;
use Itgro\LaravelConfirmations\Exceptions\UserMismatch;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * @property string id
 * @property int|null user_id
 * @property Model|null user
 * @property string action
 * @property string deliver_via
 * @property string deliver_to
 * @property string token
 * @property array options
 * @property-read int ttl_in_minutes
 * @property-read bool is_expired
 * @property-read bool was_executed
 * @property-read bool is_first_execution
 * @property-read bool can_be_executed
 * @property-read Channel channel
 * @property Carbon expires_at
 * @property Carbon executed_at
 * @property Carbon created_at
 * @property Carbon updated_at
 * @method Builder notExecuted()
 */
class Confirmation extends Model
{
    use HasRedirect, HasScopes, HasAccessors, HasRestrictions;

    public const TYPE = 'generic';
    public const TTL_IN_MINUTES = 60 * 24 * 7;
    public const CAN_BE_EXECUTED_ONLY_ONCE = true;
    public const LOGS_USER_IN = true;
    protected const JOB = null;

    protected static $actionsMap = [];
    protected static $channelsMap = [];

    public $incrementing = false;
    public $keyType = 'string';
    public $casts = [
        'options' => 'json',
        'expires_at' => 'datetime',
        'executed_at' => 'datetime',
    ];
    public $visible = [
        'id',
        'action',
        'deliver_via',
    ];

    /**
     * @param string[]|string $actions
     * @throws ConfigurationError
     */
    public static function registerAction($actions): void
    {
        foreach (\Illuminate\Support\Arr::wrap($actions) as $class) {
            static::checkConfirmationClass($class);

            static::$actionsMap[constant($class . '::TYPE')] = $class;
        }
    }

    /**
     * @param string[]|string $channels
     * @throws ConfigurationError
     */
    public static function registerChannel($channels): void
    {
        foreach (Arr::wrap($channels) as $class) {
            static::checkChannelClass($class);

            static::$channelsMap[constant($class . '::IDENTIFIER')] = $class;
        }
    }

    public static function makeChannel(?string $identifier = null): ?Channel
    {
        $channel = Arr::get(static::$channelsMap, $identifier);

        if (!$channel) {
            throw ConfigurationError::channelNotRegistered($identifier);
        }

        return app()->make($channel);
    }

    /**
     * @param string $type
     * @return string
     * @throws ConfigurationError
     */
    public static function getClass(string $type): string
    {
        $class = Arr::get(static::$actionsMap, $type, '');

        static::checkConfirmationClass($class);

        return $class;
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('action', function (Builder $builder) {
            if (static::TYPE !== self::TYPE) {
                $builder->where('action', static::TYPE);
            }
        });
    }

    /**
     * @param Channel|string $channel
     * @param Model|string $contact
     * @param array $options
     * @return Confirmation
     * @throws ConfigurationError
     */
    protected static function makeBase($channel, $contact, array $options = []): self
    {
        if (is_string($channel)) {
            $channel = static::makeChannel($channel);
        } else {
            static::checkChannelClass(get_class($channel));
        }

        if (is_string($contact)) {
            $user = null;
        } else {
            $user = $contact;

            static::checkUserModelType($user);

            $contact = $channel->getContact($user);
        }

        if (!$contact) {
            throw new InvalidContact;
        }

        $model = new static;

        $model->id = static::generateId();
        $model->action = static::TYPE;
        $model->options = static::prepareOptions($options);
        $model->deliver_via = $channel->getIdentifier();
        $model->deliver_to = $contact;
        $model->token = $channel->generateToken();

        $model->resetExpiration(false);

        $model->user()->associate($user);

        $model->save();

        return $model;
    }

    protected static function prepareOptions(array $options): array
    {
        $prepared = [];

        foreach ($options as $name => $value) {
            $prepared[$name] = static::prepareOption($name, $value);
        }

        return $prepared;
    }

    protected static function prepareOption(string $name, $value)
    {
        $method = camel_case("prepare_{$name}_option");

        if (method_exists(static::class, $method)) {
            $value = static::$method($value);
        }

        return $value;
    }

    protected static function generateId(): string
    {
        // TODO: Проверять на уникальность

        /** @noinspection PhpUnhandledExceptionInspection */
        return Uuid::uuid4()->toString();
    }

    public function getTable()
    {
        // Нужно явно указать название таблицы, чтобы потомки класса пытались сохраниться туда, куда нужно
        return config('confirmations.table');
    }

    public function newFromBuilder($attributes = [], $connection = null)
    {
        $type = Arr::get($attributes, 'action', self::TYPE);

        $class = static::getClass($type);

        /** @var static $model */
        $model = new $class();

        $model->exists = true;

        $model->setConnection($connection ?: $this->getConnectionName());

        $model->setRawAttributes((array)$attributes, true);

        $model->fireModelEvent('retrieved', false);

        return $model;
    }

    public function resolveRouteBinding($id)
    {
        $model = $this->findOrFail($id);

        return static::newFromBuilder($model->original, $model->getConnectionName());
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(config('confirmations.models.user'));
    }

    public function getChannelAttribute(): Channel
    {
        return static::makeChannel($this->deliver_via);
    }

    public function send(): bool
    {
        return $this
            ->resetTokenIfExpired()
            ->resetExpiration()
            ->channel->send($this);
    }

    public function resetExpiration(bool $persist = true): self
    {
        $this->expires_at = $this->getExpirationDate();

        if ($persist) {
            $this->save();
        }

        return $this;
    }

    public function resetTokenIfExpired(): self
    {
        if (!$this->is_expired) {
            return $this;
        }

        return $this->resetToken(true);
    }

    public function resetToken(bool $persist = true): self
    {
        $this->token = $this->channel->generateToken();

        if ($persist) {
            $this->save();
        }

        return $this;
    }

    /** @noinspection PhpDocRedundantThrowsInspection
     * @throws AlreadyExecuted
     * @throws Expired
     * @throws UserMismatch
     * @throws Throwable
     */
    public function execute(bool $logUserIn = true): bool
    {
        try {
            $this->checkThatCanBeExecuted();
            $this->checkIfExpired();

            if ($logUserIn) {
                $this->logUserIn();
            }

            event(new IsExecuting($this));

            $this->executed_at = Carbon::now();
            $this->save();

            if ($job = $this->getJob()) {
                $result = dispatch_now($job);
            } else {
                $result = true;
            }

            if ($result) {
                event(new WasExecuted($this));
            } else {
                event(new Failed($this));
            }

            return (bool)$result;
        } catch (Throwable $exception) {
            event(new Failed($this, get_class($exception)));

            /** @noinspection PhpUnhandledExceptionInspection */
            throw $exception;
        }
    }

    protected function getExpirationDate(): Carbon
    {
        return Carbon::now()->addMinutes($this->ttl_in_minutes);
    }

    /**
     * @throws UserMismatch
     */
    protected function logUserIn(): void
    {
        if (!static::LOGS_USER_IN) {
            return;
        }

        $this->checkIfUserCanBeLoggedIn();

        auth()->loginUsingId($this->user_id);
    }

    protected function getJob()
    {
        if (static::JOB && class_exists(static::JOB)) {
            $class = static::JOB;

            if ($this->user) {
                return new $class($this->user, $this->options);
            } else {
                return new $class($this->options);
            }
        }

        return null;
    }
}
