<?php

namespace Itgro\LaravelConfirmations\Models;

use Illuminate\Database\Eloquent\Model;
use Itgro\LaravelConfirmations\Channels\Contract;
use Itgro\LaravelConfirmations\Exceptions\AlreadyExecuted;
use Itgro\LaravelConfirmations\Exceptions\ConfigurationError;
use Itgro\LaravelConfirmations\Exceptions\Expired;
use Itgro\LaravelConfirmations\Exceptions\UserMismatch;

trait HasRestrictions
{
    protected static function isValidConfirmationClass(string $class): bool
    {
        return (
            $class &&
            class_exists($class) &&
            is_a($class, Confirmation::class, true) &&
            defined($class . '::TYPE') &&
            constant($class . '::TYPE')
        );
    }

    protected static function isValidChannelClass(string $class): bool
    {
        return (
            $class &&
            class_exists($class) &&
            is_a($class, Contract::class, true) &&
            defined($class . '::IDENTIFIER') &&
            constant($class . '::IDENTIFIER')
        );
    }

    protected static function isValidUserClass($user): bool
    {
        return is_null($user) || is_a($user, config('confirmations.models.user'));
    }

    /**
     * @param string $class
     * @throws ConfigurationError
     */
    protected static function checkConfirmationClass(string $class): void
    {
        if (!static::isValidConfirmationClass($class)) {
            throw ConfigurationError::invalidConfirmationClass($class);
        }
    }

    /**
     * @param string $class
     * @throws ConfigurationError
     */
    protected static function checkChannelClass(string $class)
    {
        if (!static::isValidChannelClass($class)) {
            throw ConfigurationError::invalidChannelClass($class);
        }
    }

    /**
     * @param mixed $user
     * @throws ConfigurationError
     */
    protected static function checkUserModelType($user): void
    {
        if (!static::isValidUserClass($user)) {
            throw ConfigurationError::invalidUserObject($user);
        }
    }

    public function authorize($user = null): bool
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        static::checkUserModelType($user);

        if (!$this->user_id) {
            return true;
        }

        $user = $user ?? auth()->user();

        return !$user || $user->id === $this->user_id;
    }

    /**
     * @throws AlreadyExecuted
     */
    protected function checkThatCanBeExecuted(): void
    {
        if (!$this->can_be_executed) {
            throw new AlreadyExecuted();
        }
    }

    /**
     * @throws Expired
     */
    protected function checkIfExpired(): void
    {
        if ($this->is_expired) {
            throw new Expired;
        }
    }

    /**
     * @param Model|null $user
     * @throws UserMismatch
     */
    protected function checkIfUserCanBeLoggedIn($user = null): void
    {
        if (!$this->authorize($user)) {
            throw new UserMismatch;
        }
    }
}
