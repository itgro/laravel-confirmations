<?php

namespace Itgro\LaravelConfirmations\Models;

class Redirect extends Confirmation
{
    public const TYPE = 'redirect';
    public const TTL_IN_MINUTES = 60 * 24 * 100;
    public const CAN_BE_EXECUTED_ONLY_ONCE = false;
}
