<?php

namespace Itgro\LaravelConfirmations\Models;

use Illuminate\Database\Eloquent\Builder;

trait HasScopes
{
    public static function scopeNotExecuted(Builder $query): Builder
    {
        return $query->whereNull('executed_at');
    }
}
