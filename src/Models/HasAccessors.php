<?php

namespace Itgro\LaravelConfirmations\Models;

trait HasAccessors
{
    public function getIsExpiredAttribute(): bool
    {
        return $this->expires_at && $this->expires_at->isPast();
    }

    public function getWasExecutedAttribute(): bool
    {
        return !$this->is_first_execution;
    }

    public function getTtlInMinutesAttribute(): int
    {
        return static::TTL_IN_MINUTES;
    }

    public function getCanBeExecutedAttribute(): bool
    {
        return !static::CAN_BE_EXECUTED_ONLY_ONCE || $this->is_first_execution;
    }

    public function getIsFirstExecutionAttribute(): bool
    {
        return !$this->executed_at;
    }
}
