<?php

namespace Itgro\LaravelConfirmations\Models;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;

/**
 * @property string redirectRoute
 * @property array redirectParameters
 * @property string redirectAnchor
 * @property string defaultRedirectRoute
 * @property array defaultRedirectParameters
 * @property string defaultRedirectAnchor
 */
trait HasRedirect
{
    public function redirect(): RedirectResponse
    {
        $url = route($this->redirectRoute, $this->redirectParameters);
        $anchor = $this->redirectAnchor;

        return redirect($url . ($anchor ? "#{$anchor}" : ''));
    }

    public function getDefaultRedirectRouteAttribute(): string
    {
        return config('confirmations.default_redirect.route', 'home');
    }

    public function getDefaultRedirectParametersAttribute(): array
    {
        return config('confirmations.default_redirect.parameters', []);
    }

    public function getDefaultRedirectAnchorAttribute(): string
    {
        return config('confirmations.default_redirect.anchor', '');
    }

    public function getRedirectRouteAttribute(): string
    {
        return Arr::get($this->options, 'redirect.route', $this->defaultRedirectRoute);
    }

    public function getRedirectParametersAttribute(): array
    {
        return Arr::get($this->options, 'redirect.parameters', $this->defaultRedirectParameters);
    }

    public function getRedirectAnchorAttribute(): string
    {
        return Arr::get($this->options, 'redirect.anchor', $this->defaultRedirectAnchor);
    }

    public function setRedirectRouteAttribute($value): self
    {
        Arr::set($this->options, 'redirect.route', $value);

        return $this;
    }

    public function setRedirectParametersAttribute(array $value): self
    {
        Arr::set($this->options, 'redirect.parameters', $value);

        return $this;
    }

    public function setRedirectAnchorAttribute($value): self
    {
        Arr::set($this->options, 'redirect.anchor', $value);

        return $this;
    }
}
