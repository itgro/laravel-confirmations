<?php

Route::group([
    'as' => 'confirmations::',
    'prefix' => config('confirmations.routes.prefix'),
    'middleware' => config('confirmations.routes.middleware'),
], function () {
    Route::get('{confirmation}', config('confirmations.routes.controller') . '@form')
        ->name('form')
        ->middleware(config('confirmations.routes.form.middleware'));

    Route::post('{confirmation}', config('confirmations.routes.controller') . '@check')
        ->name('check')
        ->middleware(config('confirmations.routes.check.middleware'));

    Route::post('{confirmation}/send', config('confirmations.routes.controller') . '@send')
        ->name('send')
        ->middleware(config('confirmations.routes.send.middleware'));
});
