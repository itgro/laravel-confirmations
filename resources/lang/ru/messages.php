<?php

use Itgro\LaravelConfirmations\Models\Confirmation;
use Itgro\LaravelConfirmations\Models\Redirect;

$genericMessages = [
    'success' => 'Действие подтверждено!',
    'failure' => 'Не удалось подтвердить действие.',

    'already_executed' => 'Данный код уже был использован.',
    'expired' => 'Истёк срок выполнения действия.',
    'access_denied' => 'Доступ к действию запрещён.',
    'unexpected_failure' => 'Не удалось выполнить действие. :message',

    'form_title' => 'Подтверждение',
    'form_token' => 'Код',
    'form_token_placeholder' => '',
    'form_submit' => 'Отправить',
    'form_resend' => 'Выслать повторно',
    'form_resend_delay' => 'Повторная отправка возможна через :delay',
];

return [
    'sent' => 'Сообщение отправлено.',
    'not_sent' => 'Не удалось отправить сообщение.',

    Confirmation::TYPE => $genericMessages,
    Redirect::class => array_merge($genericMessages, ['success' => '']),
];
