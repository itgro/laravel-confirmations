@extends('layouts.app')

@php
    $transPrefix = "confirmations::messages.{$confirmation->action}.form";
@endphp

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ trans("{$transPrefix}_title") }}
                    </div>

                    <div class="card-body">
                        <form
                                method="POST"
                                action="{{ route('confirmations::check', $confirmation) }}"
                                id="confirmation-form-{{ $confirmation->id }}"
                                aria-label="{{ trans("{$transPrefix}_title") }}"
                        >
                            @csrf

                            <div class="form-group row">
                                <label for="token" class="col-md-4 col-form-label text-md-right">
                                    {{ trans("{$transPrefix}_token") }}
                                </label>

                                <div class="col-md-6">
                                    <input
                                            id="token"
                                            type="tel"
                                            class="form-control{{ $errors->has('token') ? ' is-invalid' : '' }}"
                                            name="token"
                                            value="{{ old('token') ?? request('token') }}"
                                            placeholder="{{ trans("{$transPrefix}_token_placeholder") }}"
                                            required
                                            autofocus
                                    >

                                    @if ($errors->has('token'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('token') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ trans("{$transPrefix}_submit") }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        {{-- Если в get-параметрах есть код, значит человек пришёл по ссылке из уведомления и повторная отправка ему не нужна. --}}
                        @unless($prefilled)
                            <form method="POST" action="{{ route('confirmations::send', $confirmation) }}">
                                @csrf

                                @if($errors->has('throttle'))
                                    <div class="alert alert-danger" role="alert">
                                        <strong>{{ $errors->first('throttle') }}</strong>
                                    </div>
                                @elseif($confirmation->resendDelay > 0)
                                    <p data-delay="{{ $confirmation->resendDelay }}">
                                        {{ trans("{$transPrefix}_resend_delay", ['delay' => $confirmation->resendDelay . ' ' . choose_declension($confirmation->resendDelay, 'секунду', 'секунды', 'секунд')]) }}
                                    </p>
                                @endif

                                <button type="submit" class="btn btn-link">
                                    {{ trans("{$transPrefix}_resend") }}
                                </button>
                            </form>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($prefilled && $errors->isEmpty())
        <script type="text/javascript">
            var $form = document.querySelector('#confirmation-form-{{ $confirmation->id }}');

            if ($form) {
                $form.submit();
            }
        </script>
    @endif
@endsection
