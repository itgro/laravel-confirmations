@if($message = session()->pull('confirmations_message', null))
    <div class="alert alert-{{ $message['level'] }}" role="alert">
        {!! $message['text'] !!}
    </div>
@endif
