<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfirmationsTable extends Migration
{
    protected $table;
    protected $usersTable;
    protected $usersKey;

    public function __construct()
    {
        $this->table = config('confirmations.table');

        $userClass = config('confirmations.models.user');
        $userModel = new $userClass;

        $this->usersTable = $userModel->getTable();
        $this->usersKey = $userModel->getKeyName();
    }

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('action');
            $table->string('deliver_via');
            $table->string('deliver_to');
            $table->string('token');
            $table->json('options')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('executed_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references($this->usersKey)
                ->on($this->usersTable)
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
